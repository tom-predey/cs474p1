Every time a change is made to the source, update developers log. 
Formatting: Date, time and editor name, on next line describe the changes made to the code.
         	    Separate entries by line of '#' characters. 
###############################################################################
03/07/2016	6:29PM    Tom Predey
Added Item and Customer classes with constructors and relevant getter methods.
Not entirely sure at this point that we'll keep them exactly the way they are,
but it's a good start at least.
###############################################################################
03/07/2016      9:07PM    Tom Predey
Added Products class, which is a mock database. I used a HashSet for the 
implementation of the database, which won't allow duplicate items into it. This
means we'll probably have to override the equals method in the Item class. The 
Products class has a default constructor and a user specified one. This should 
make testing easier, but will allow users to add more text cases if that's what
they want to do. Also added Shopping Cart class, put in a TODO for the 
calcPurchasePrice method. We'll get to that one last, just in case we decide to
change up some stuff in the meanwhile. Will add jUnit tests for the other 
classes first.
###############################################################################
03/13/2016      9:04PM    Tom Predey
Equals method overridden for Item class and documentation written. Compares
only the itemID of two Item objects, which seems like the proper way of doing 
it from the user story. Might be unnecessary, but that can be determined upon 
review.
###############################################################################
03/13/2016	9:48PM    Tom Predey
Wrote most of the the test for the Customer class. Needs a few things changed
though. I'm going to change the tax field of the customer class to be boolean
and have the float value of tax applied in the shopping cart. Also need to 
figure out how to import classes from another directory into my test class. I
would usually google this but I'm on an airplane so that is off-limits.
###############################################################################
03/14/2016      3:51PM	  Tom Predey
Finished test for Customer class, added TestRunner class to run JUnit tests in
the terminal. Some IDEs have JUnit built in and don't require the runner file, 
but I did. CustomerTest tests all passed, still need to do analysis about the 
degree of coverage, but I think I covered all cases. 
###############################################################################
03/14/2016      11:10PM   Tom Predey
Finished testing ItemTest class, all tests are passing. Added ItemTest to the
TestRunner.java file. Completed ShoppingCart class and in the process of 
writing the tests for it. Very long testing file for the ShoppingCart class 
required due to the many branches, equivalence partitions, and boundaries. I 
have also gone back and added documentation to classes where it is important. 
I'm beginning to run out of time, but there are a handful of things left to do
before this project can be marked as complete: writing tests for the mock 
database, updating the UML diagram to reflect changes made in the project 
structure, and ensuring that the documentation is adequate.
###############################################################################
03/15/2016      1:03PM    Tom Predey
Added test class for Products, all tests passing. Also completed tests for 
ShoppingCart class, all tests passing. JUnit note: When using assertEquals 
(which I think has actually been deprecated, but it worked okay), to compare 
two floating point numbers, you need to specify a "delta" value that indicates
the margin of error between two floating point numbers that will still be 
considered equal. I made this a super small value. Something else that might be
worth noting is that I sort of included a database test intrinsic to one of the
test cases in ShoppingCartTest.java. Indicated with a comment in the source 
code, I covered the case for which there is a product ID that does not 
correspond to any of the products in the database. It essentially ignores the 
item altogether and does not include the item in the count of total items. This
is an important consideration for discounts. I finished the TestRunner.java
class, which gives more information about the tests being run (indicating which
ones passed altogether, making debugging easier). 
###############################################################################

