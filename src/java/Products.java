//THIS CLASS SIMULATES A DATABASE
import java.util.Iterator;
import java.util.HashSet;

public class Products{
    protected HashSet<Item> data;
    
    //Default constructor -- initializes the hash set with 15 Items
    public Products(){
        this.data = new HashSet<Item>();
        for (int i = 1; i <= 15; i++){
            Item tmp = new Item("prd"+ new Integer(i).toString(), (float) 10.00);
            data.add(tmp);
        }
    }
    
    //User defined constructor -- allows the user to import any HashSet of Items.
    public Products(HashSet<Item> data){
        this.data = data;
    }

    //Checks if item with matching itemID is contained in the database and returns it
    //If the matching item is not found, it returns null. 
    public Item getItem(String itemID){
        Iterator<Item> iterator = data.iterator();
        while (iterator.hasNext()){
            Item tmp = iterator.next();
            if (tmp.getItemID().equals(itemID))
                 return tmp;
        }
        return null;
    }
}
