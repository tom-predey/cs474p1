
public class Item{
    protected String itemID; //String item identifier
    protected float itemPrice; //Item price in dollars, unrounded

    public Item(String itemID, float itemPrice){
        this.itemID = itemID; 
        this.itemPrice = itemPrice;
    }

    public String getItemID(){
        return itemID;
    }

    public float getPrice(){
        return itemPrice;
    }

    @Override
    public boolean equals(Object o){
        if (o == null) //java convention makes two null objects unequal
            return false;
        if (!Item.class.isAssignableFrom(o.getClass())) // o must be same class or subclass
            return false; 
        final Item i = (Item) o; //if it gets this far, o can be cast as Item
        if ((this.itemID == null) ? (i.itemID != null) : !this.itemID.equals(i.itemID))
         //checks if current item in question is null object, if not then it compares them
            return false;
        if (!this.itemID.equals(i.itemID)) //compares String itemID
            return false;
        return true;
    }  
}
