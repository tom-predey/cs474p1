
import org.junit.*;
import static org.junit.Assert.*;

public class CustomerTest{
    
    Customer c1, c2, c3, c4;
    
    @Before
    public void setUp() throws Exception{
        c1 = new Customer(false, false); //Non-member, non-tax-exempt
        c2 = new Customer(false, true);  //Non-member, tax-exempt
        c3 = new Customer(true, false);  //Member, non-tax-exempt
        c4 = new Customer(true, true);   //Member, tax-exempt
    }

    @After
    public void tearDown() throws Exception{
        c1 = null;
        c2 = null;
        c3 = null;
        c4 = null;
    }

    @Test
    public void c1Test(){
        assertFalse(c1.isMember());
        assertFalse(c1.isTaxExempt());
    }
 
    @Test
    public void c2Test(){
        assertFalse(c2.isMember());
        assertTrue(c2.isTaxExempt());
    }
 
    @Test
    public void c3Test(){
        assertTrue(c3.isMember());
        assertFalse(c3.isTaxExempt());
    }
 
    @Test
    public void c4Test(){
        assertTrue(c4.isMember());
        assertTrue(c4.isTaxExempt());
    }
} 
