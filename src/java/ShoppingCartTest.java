//TODO Test for ShoppingCart class

import org.junit.*;
import static org.junit.Assert.*;

public class ShoppingCartTest{
    
    //Basic products set, containing 15 unique items
    private Products data = new Products();
    private final float DELTA = (float) 0.0000000001; 
    ShoppingCart s;
    Customer c1, c2, c3, c4; 
    String [] pid0, pid1, pid2, pid3, pid4, pid5, pid6;
    
    @Before
    public void setUp(){
        
        pid0 = new String[0]; //Covers empty shopping cart case

        pid1 = new String[]{"prd1", "prd2", "prd3"}; //Covers 1-4 item region
        
        pid2 = new String[]{"prd1", "prd2", "prd3", "prd4", "prd5"}; //Boundary case

        pid3 = new String[]{"prd0", "prd1", "prd2", "prd3", "prd4", "prd5", "prd6", "prd7"}; // Covers 5-9 item region
        
        pid4 = new String[]{"prd1", "prd2", "prd3", "prd4", "prd5", "prd6", "prd7", "prd8", "prd9", "prd10"}; //Boundary Case

        pid5 = new String[]{"prd1", "prd2", "prd3", "prd4", "prd5", "prd6", "prd7", "prd8", "prd9", "prd10", "prd11"}; //covers 10-50 item region

        pid6 = new String[]{ "prd1", "prd2", "prd3", "prd4", "prd5", "prd6", "prd7", "prd8", "prd9", "prd10", "prd1", "prd2", "prd3", "prd4", "prd5", "prd6", "prd7", "prd8", "prd9", "prd10","prd1", "prd2", "prd3", "prd4", "prd5", "prd6", "prd7", "prd8", "prd9", "prd10", "prd1", "prd2", "prd3", "prd4", "prd5", "prd6", "prd7", "prd8", "prd9", "prd10", "prd1", "prd2", "prd3", "prd4", "prd5", "prd6", "prd7", "prd8", "prd9", "prd10", "prd1", "prd2", "prd3", "prd4", "prd5", "prd6", "prd7", "prd8", "prd9", "prd10"}; //Covers 50+ item case  
         
        c1 = new Customer(false, false); //Non-member, non-tax-exempt
        c2 = new Customer(false, true);  //Non-member, tax-exempt
        c3 = new Customer(true, false);  //Member, non-tax-exempt
        c4 = new Customer(true, true);   //Member, tax-exempt
        
        s = new ShoppingCart(data); //Shopping cart object uses default mock database
}
    @After
    public void tearDown(){
        c1 = null;
        c2 = null;
        c3 = null;
        c4 = null;
        pid1 = null;
        pid2 = null;
        pid3 = null;
        pid4 = null; 
        pid5 = null;
        pid6 = null;
        s = null;
    }
    @Test
    public void testNullCases(){

        assertNull(s.calcPurchasePrice(pid1, null )); //null case, item list is only a placeholder    
        assertNull(s.calcPurchasePrice(pid6, c1)); //Customer only placeholder, will never use c1
    }
    
    @Test
    //Tests empty product id list
    public void testpid0c1(){
        //For pid0 and c1
        float [] tmp = s.calcPurchasePrice(pid0, c1);
        assertEquals(tmp[0], (float) 0.00, DELTA);
        assertEquals(tmp[1], (float) 0.00, DELTA);
        assertEquals(tmp[2], (float) 0.00, DELTA);
        assertEquals(tmp[3], (float) 0.00, DELTA);
    }
        
    //Customer tests, no Item discounts
    @Test
    public void testpid1c1(){
        //For pid1 and c1
        float [] tmp = s.calcPurchasePrice(pid1, c1);
        assertEquals(tmp[0], (float) 30.00, DELTA);
        assertEquals(tmp[1], (float) 0.00, DELTA);
        assertEquals(tmp[2], (float) 1.35, DELTA);
        assertEquals(tmp[3], (float) 31.35, DELTA);
    }
    @Test
    public void testpid1c2(){
        //For pid1 and c2
        float [] tmp = s.calcPurchasePrice(pid1, c2);
        assertEquals(tmp[0], (float) 30.00, DELTA);
        assertEquals(tmp[1], (float) 0.00, DELTA);
        assertEquals(tmp[2], (float) 0.00, DELTA);
        assertEquals(tmp[3], (float) 30.00, DELTA);
    }
    @Test
    public void testpid1c3(){
        //For pid1 and c3
        float [] tmp = s.calcPurchasePrice(pid1, c3); 
        assertEquals(tmp[0], (float) 30.00, DELTA);
        assertEquals(tmp[1], (float) 3.00, DELTA);
        assertEquals(tmp[2], (float) 1.22, DELTA);
        assertEquals(tmp[3], (float) 28.22, DELTA);
    }
    @Test
    public void testpid1c4(){
        //For pid1 and c4
        float [] tmp = s.calcPurchasePrice(pid1, c4);
        assertEquals(tmp[0], (float) 30.00, DELTA);
        assertEquals(tmp[1], (float) 3.00, DELTA);
        assertEquals(tmp[2], (float) 0.00, DELTA);
        assertEquals(tmp[3], (float) 27.00, DELTA);
    }
    
    //Boundary condition values, single discount
    @Test
    public void testpid2c1(){
        //For pid2 and c1
        float [] tmp = s.calcPurchasePrice(pid2, c1);
        assertEquals(tmp[0], (float) 50.00, DELTA);
        assertEquals(tmp[1], (float) 2.50, DELTA);
        assertEquals(tmp[2], (float) 2.14, DELTA);
        assertEquals(tmp[3], (float) 49.64, DELTA);
    }
    @Test
    public void testpid4c2(){
        //For pid4 and c2
        float [] tmp = s.calcPurchasePrice(pid4, c2);
        assertEquals(tmp[0], (float) 100.00, DELTA);
        assertEquals(tmp[1], (float) 10.00, DELTA);
        assertEquals(tmp[2], (float) 0.00, DELTA);
        assertEquals(tmp[3], (float) 90.00, DELTA);
    }

    //Item discount, Single Discount
    @Test
    public void testpid3c1(){
        //For pid3 and c1
        float [] tmp = s.calcPurchasePrice(pid3, c1);
        assertEquals(tmp[0], (float) 70.00, DELTA);
        assertEquals(tmp[1], (float) 3.50, DELTA);
        assertEquals(tmp[2], (float) 2.99, DELTA);
        assertEquals(tmp[3], (float) 69.49, DELTA);
    }
    @Test
    public void testpid5c2(){
        //For pid5 and c2
        float [] tmp = s.calcPurchasePrice(pid5, c2);
        assertEquals(tmp[0], (float) 110.00, DELTA);
        assertEquals(tmp[1], (float) 11.00, DELTA);
        assertEquals(tmp[2], (float) 0.00, DELTA);
        assertEquals(tmp[3], (float) 99.00, DELTA);
    }

    //Compound Discounts
    public void testpid3c3(){
	//For pid3 and c3
	float [] tmp = s.calcPurchasePrice(pid3, c3); 
	assertEquals(tmp[0], (float) 70.00, DELTA);
	assertEquals(tmp[1], (float) 11.15, DELTA);
	assertEquals(tmp[2], (float) 2.69, DELTA);
	assertEquals(tmp[3], (float) 61.54, DELTA);
    }

    public void testpid5c4(){
	//For pid5 and c4
	float [] tmp = s.calcPurchasePrice(pid5, c4);
	assertEquals(tmp[0], (float) 110.00, DELTA);
	assertEquals(tmp[1], (float) 20.90, DELTA);
	assertEquals(tmp[2], (float) 0.00, DELTA);
	assertEquals(tmp[3], (float) 89.10, DELTA);
    }
    
}    
