import org.junit.*;
import static org.junit.Assert.*;
import java.util.HashSet;
public class ProductsTest{
    
    Products p1, p2;

    @Before
    public void setUp(){
        p1 = new Products(); //default constructor
        HashSet<Item> h = new HashSet<Item>();
        h.add(new Item("id1", (float) 5.00)); 
        h.add(new Item("id2", (float) 6.00));
        h.add(new Item("id3", (float) 91.00));
        h.add(new Item("id4", (float) 2.00));
        p2 = new Products(h);
    }

    @After
    public void tearDown(){
        p1 = null;
        p2 = null;
    }

    @Test
    public void testDefault(){
        Item tmp = new Item("prd1", (float) 100.00);
        assertNull(p1.getItem("prd27"));
        assertEquals(p1.getItem("prd1"), tmp);
    }

    @Test
    public void testUserDefined(){
        Item tmp = new Item("id3", (float) 90.00);
        assertNull(p2.getItem("prd2"));
        assertTrue(p2.getItem("id3").equals(tmp));
    }
}
