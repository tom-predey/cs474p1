public class Customer{
    protected boolean membership; //True if customer has membership, false otherwise
    protected boolean taxExempt; //True if tax exempt, false otherwise
    
    public Customer(boolean membership, boolean taxExempt){
        this.membership = membership;
        this.taxExempt = taxExempt;
    }
    
    public boolean isMember(){
        return membership;
    }

    public boolean isTaxExempt(){ 
        return taxExempt;
    }
}
