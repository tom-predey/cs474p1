
import org.junit.*;
import static org.junit.Assert.*;

public class ItemTest{
    
    Item i1, i2, i3;

    @Before
    public void setUp() throws Exception{
        i1 = new Item("i1strID", (float) 4.00);
        i2 = new Item("i2strID", (float) 4.00);
        i3 = new Item("i1strID", (float) 4.50);
    }

    @After
    public void tearDown() throws Exception{
        i1 = null;
        i2 = null;
        i3 = null;
    }

    @Test
    public void i1test(){
        assertEquals(i1.getItemID(), "i1strID");
        assertTrue(i1.getPrice() == (float) 4.00);
    }

    @Test
    public void comparisonTest(){
        assertTrue(i1.equals(i3));
        assertFalse(i1.equals(i2));
        assertTrue(i1.getPrice() != i3.getPrice());
    }
}
