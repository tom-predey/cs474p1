import java.math.BigDecimal;

public class ShoppingCart{
    protected Products data;
    
    protected final float taxRate = (float) 0.045;
    public ShoppingCart(Products data){
        this.data = data;
    }

    //Helper method to round to two decimal places
    public float round2decimal(float d){
        BigDecimal bd = new BigDecimal(Float.toString(d));
        bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);
        return bd.floatValue();
    } 

    public float[] calcPurchasePrice(String [] pid, Customer c){
        //TODO Fill in this method
        if (pid.length > 50){
            System.out.println("Too many items in shopping cart, please remove items before checking out");
            return null;
        }
        else if(pid.length < 0){
            System.out.println("Invalid number of items in the shopping cart.");
            return null;
        }
        else if(c == null){
            System.out.println("Not a valid customer");
            return null;
        }
        else{
            //Calculates the base price before taxes and discounts
            float basePrice = (float) 0.0;
            int validItemCount = 0;
            for (int i = 0; i < pid.length; i++){
                if (data.getItem(pid[i])!=null){
                    basePrice += data.getItem(pid[i]).getPrice(); //gets prices from database
                    validItemCount++; //Only items found in the database will factor into discounts
                }
            }

            //Calculates the discount
            float discountPrice = basePrice;
            if (validItemCount >=5 && validItemCount < 10){
                discountPrice = (float) 0.95*basePrice;
            }
            else if (validItemCount >= 10 && validItemCount <=50){
                discountPrice = (float) 0.90*basePrice;
            }
            
            if (c.isMember()){
                discountPrice = (float) 0.90*discountPrice;
            }
            float discount = basePrice - discountPrice;
            //Round values before tax is applied
            basePrice = round2decimal(basePrice);
            discount = round2decimal(discount);
            
            //Calculates after-tax price when applicable
            float totalPrice = discountPrice;
            float taxAmount = (float) 0.0;
	    if (!c.isTaxExempt()){
                taxAmount = (float) taxRate*discountPrice;
                totalPrice = taxAmount+totalPrice;
            }
            
            //Round after tax valu
            taxAmount = round2decimal(taxAmount);
            totalPrice = round2decimal(totalPrice);
            
            //Relevant quantities stored in float array size four
            //these are the initial net purchase price, total discount, tax paid, and total price 
            float [] prices = {basePrice, discount, taxAmount, totalPrice};
            return prices;
        }
    }
}
