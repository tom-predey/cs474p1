# README #

## Program Summary ##

### Class Summary ###

This is a simulation of an online shopping cart. The primary function of this program is to take a customer's shopping cart, access a database to get the items stored in the cart, and compute the price payed by the designated customer. Therefore the ShoppingCart class is dependent on three other classes to perform its functionality. These are (i) The Customer class, which provides information about the customer's tax status and whether he or she is a member of the store's discount shopper program, (ii) the Item class, which provides identifying information (the Item's string ID) as well as the associated price of this item, (iii) the Products mock database, for which more information is available below in the README. Using the elements from these three classes, the ShoppingCart class implements a method calcPurchasePrice, which conditionally determines the varied components of the total price the customer must pay. 

### Test Summary ###

Comprehensive unit testing was performed on each class in this project using JUnit 1.5. Many of these tests were simple, just making sure the constructors acted appropriately and the getter methods worked as designed. As expected, the most involved tests were associated with the ShoppingCart class. This was the most complicated part of the project, and many tests were required for consistent coverage. I'd like to go over which tests I chose to include and which tests I chose to exclude -- though full branch coverage would have required 24 (or more) different configurations of calcPurchasePrice, I will argue that relevant coverage only required 12. 

It is important to test the null input and zero/50+ item cases, so I tested those first. These tests required me to add conditions to the calcPurchasePrice methods to ensure that a null result with an error message was returned for all illegal cases. The zero item test also ensured that I wasn't initializing any of the method variables to have values they shouldn't. I then tested normal operation, first using every customer type and no 'full cart' discounts. From here, I tested boundaries of the discount regions as well as id lists from the equivalence partitions. Included in these were behavior tests for itemIDs not found in the database. I then tested some "compound discounts" to ensure that the math was correct for some of the more complicated discount calculations. Bugs exposed by these tests were as follows:

* testpid1c3 and testpid1c4 exposed a bug in the calculation of membership discounts - it was creating discounts over the entire price of the object.

* testpid2c1 exposed that the calcPurchasePrice method was not rounding the tax amount.

* testpid3c1 exposed an error in the testing - I had meant to include an itemID that did not have a corresponding Item object in the database, but the itemID that I included actually did have one. So I adjusted the itemID to test that the code would work well for it.

What is clear later on in the testing, however, is that I did not test every equivalence partition with every customer type, most notably when accounting for the taxes. After analysis of the calcPurchasePrice method, it is clear that we do not have to test the tax calculations for every single case - because of their dependency on the discount calculations, after the tax calculation is correct for *any* shopping cart, the tax calculation error could only depend on an error in the discount calculations. By eliminating some of the tax calculation tests, I was able to cut the number of tests required to verify this program by half. 

## How do I get set up? ##

### Summary of set up ###

Use associated makefile to compile tests

### Configuration ###

See UML Class diagram for project configuration

### Database configuration ###

Mock database of Item objects. Simulated using HashSet<Item> and database getItem(String id) method.

### How to run tests ###

Compile and run TestRunner.java in the command line or use IDE built in features to compile and run tests. For running one's own Black-Box testing, see the UML Class diagram for the relevant information about the class and method signatures and the above class summary for more details about the implementation. 

### Who do I talk to? ###

* Talk to Tom Predey, Osamah Albayati, or Ogor Ifuwe with any questions about the contents of this project